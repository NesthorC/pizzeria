@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenido</div>

                <div class="panel-body">
                @if(Auth::user()->hasRole('admin'))
                <h5 class="card-title">Pedidos</h5>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pizzas</th>
                            <th scope="col">Usuario</th>
                            <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row">1</th>
                            <td>Peperoni,huwainana,Mexicana</td>
                            <td>empleado1</td>
                            <td>$325</td>
                            </tr>
                            <tr>
                            <th scope="row">2</th>
                            <td>huwainana,Mexicana</td>
                            <td>empleado 2</td>
                            <td>$215</td>
                            </tr>
                            
                        </tbody>
                        </table>
                @else
                    <div>Acceso Empleado</div>
                @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
